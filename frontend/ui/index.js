const electron = require('electron');
const app = electron.app;
const BrowserWindow = electron.BrowserWindow;
const fs = require('fs');
const Child = require('./emotion_py');
var child = new Child(['../core/dl/startup.py'])


var mainWindow;

app.on('window-all-closed', function() {
  if (process.platform != 'darwin') {
      app.quit();
      child.kill();
  }
});

app.on('ready', function() {
  child.start();
  mainWindow = new BrowserWindow({width: 1024, height: 720, frame:true});
  //mainWindow.openDevTools();
  mainWindow.loadURL('file://' + __dirname + '/views/webview.html');
  // mainWindow.webContents.once('dom-ready', function() {
  //
  // });
  mainWindow.on('closed', function() {
    mainWindow = null;
  });
});
