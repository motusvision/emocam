updateEmotion = function(_emotion) {
    console.log(_emotion)
    if(document.getElementById("vid_frame"))
    {
        fetchNewVideoList(_emotion)
    }

    function fetchNewVideoList(_emotion) {
        var requestText =
        {
             emotion: _emotion
        }

        var requestData = JSON.stringify(requestText);
        $.ajax({
          type: "POST",
          contentType: "application/json",
          url: "http://localhost:8080/emotion",
          data: requestData,
          dataType: 'json',
          timeout: 100000,
          xhrFields: {withCredentials: true},
          beforeSend: function (jqXHR, settings) {
              console.log(settings.url);
          },
          success: function (data) {
              console.log("SUCCESS");
              var str = data.toString()
              var arr = str.split(',')
              //console.log(arr)
              updateVideoList(arr)
          },
          error: function (e) {
              console.log("ERROR: " + e);
          },
          done: function (e) {
              console.log("DONE");
          }
        });
    }

    function updateVideoList(videoList) {
        src = "http://www.youtube.com/embed/" + videoList[0] + "?rel=0&amp;showinfo=0&amp;autohide=1";
        videoListHTML = "";
        for (var i = 0; i < videoList.length; i++) {
          var id = videoList[i]
          videoListHTML +=
              "<div class=\"vid-item\" onclick=\"document.getElementById('vid_frame').src='http://youtube.com/embed/" + id + "?autoplay=1&amp;rel=0&amp;showinfo=0&amp;autohide=1'\">" +
              "<div class=\"thumb\"><img src=\"http://img.youtube.com/vi/" + id + "/0.jpg\"></div>" +
              "<div class=\"desc\">Jessica Hernandez &amp; the Deltas - Dead Brains</div>" +
              "</div>";
        }
        document.getElementById("vid_frame").src = src;
        document.getElementById("vid_list").innerHTML = videoListHTML
    }
}

// window.onload = function() {
//     var script = document.createElement("script");
//     script.src = "https://code.jquery.com/jquery-2.1.4.min.js";
//     script.onload = script.onreadystatechange = function() {
//         $(document).ready(function() {
//
//
//         });
//     };
//     document.body.appendChild(script);
// };