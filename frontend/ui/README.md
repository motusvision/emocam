## Instructions to build frontend
#### If backend/server is ready
#### 1. Run the folowing command 
- ```npm install``` from this directory. 
- All dependencies should be installed from ```package.json```
- Note: If there is an error saying ```errno EAI_AGAIN```, it's network issue, try again.

#### 2. Run the following command to install electron globally
- ```npm install -g electron```

#### 3. Start the application 
- ```electron index.js```
- Now app is started. 