'use strict'
const ipcMain = require('electron').ipcMain;
var spawn, child, currEmotion, lastEmotion;
var updateInterval = null;
var initEmotion = 0;
class Child {
    constructor(args) {
        spawn = require('child_process').spawn;
        child = spawn('python', ['../core/dl/startup.py']);
        currEmotion = '';
		lastEmotion = '';
		initEmotion = 0;
    }

    start() {
        ipcMain.on('updatedEmotion', function (event, arg) {

            if(updateInterval !== null) {
                clearInterval(updateInterval);
            }

            updateInterval = setInterval(function(){
				if(currEmotion != '' && currEmotion != null)
				{
					console.log("Emotion suggested: " + currEmotion)
					event.sender.send('updatedEmotionReply', currEmotion);
					currEmotion = ''
				}
            }, 1000 * 5 * 1)

        });

        child.stdout.on('data', function (data) {
            var rawData = data.toString().toLowerCase();
            var regex = /\['([^,]+)',.*/g;
            var match = regex.exec(rawData);
            //console.log(match)
            //console.log(rawData)
            //console.log("#")
            var newEmotion = null;
            if (match === null) {
            } else {
				console.log(".")
                newEmotion = match[1];
            }
            if(lastEmotion !== newEmotion && newEmotion != null && newEmotion != ''){
				console.log("lastEmotion: " + lastEmotion + " newEmotion: " + newEmotion)
                lastEmotion = currEmotion = newEmotion;
				initEmotion = 1;
            }
        });

        child.stdout.on('end', function () {
            //console.log(output);
        });

        // this.child.stdin.write(JSON.stringify(this.currEmotion));
        // this.child.stdin.end();
    }

    kill(){
        child.stdin.pause();
        child.kill();
    }
}

module.exports = Child;
