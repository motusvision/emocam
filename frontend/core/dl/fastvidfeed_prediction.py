import sys

import cv2
import numpy as np
import scipy as scp
import scipy.misc
import tensorflow as tf

import emotion
import time

global WORKING_DIR, LABELS_MAPPING_FILE, TRAINED_MODEL_PATH, FACE_CASCADE_MODEL_PATH

MEAN = 128.0
SKIP_FRAMES = 1

IMAGE_WIDTH = 48
IMAGE_HEIGHT = 48
IMAGE_SIZE = 48


def readMappingFromFile(fileName):
    labelsDict = {}
    file = open(fileName)
    for line in file.readlines():
        label, key = line.strip().split(',')
        if key not in labelsDict.keys():
            labelsDict[int(key)] = label

    return labelsDict


# detects if a face is present in a image and then process the image to be passed to our model for prediction
def detectFace(frame):
    # load haar cascade
    faceCascade = cv2.CascadeClassifier(FACE_CASCADE_MODEL_PATH)
    # we take neighbors value as 5
    neighbors = 5
    # convert frame from color to grayscale
    frameGray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    # run haar cascade
    faces = faceCascade.detectMultiScale(frameGray, 1.15, neighbors)

    if (len(faces)) == 0:
        return None

    x, y, w, h = faces[0]
    # crop face area of image
    img = frameGray[y:y + h, x:x + w]
    # draw a bounding box on the face detected
    cv2.rectangle(frame, (x, y), (x + w, y + h), (255, 0, 0), 2)
    # resize image to 48*48 pixels
    if img.shape[0] != IMAGE_HEIGHT or img.shape[1] != IMAGE_WIDTH:
        img = scp.misc.imresize(img, (IMAGE_HEIGHT, IMAGE_WIDTH), interp='cubic')

    return img


def loadModelAndSession(modelPath):
    # instantiate EmotionNet class
    model = emotion.EmotionNet()
    # load TensorFlow graph
    with tf.Graph().as_default():
        # a place holder to pass a image for prediction to model
        imageTf = tf.placeholder(tf.float32, [1, IMAGE_SIZE, IMAGE_SIZE, 1], name="imagePlaceholder")
        # create a TensorFlow session
        sess = tf.InteractiveSession()
        pred = model.build(imageTf)
        # restore the saved model
        saver = tf.train.Saver()
        saver.restore(sess, modelPath)

    return sess, pred, imageTf


def main(WORKING_DIR):
    global LABELS_MAPPING_FILE, TRAINED_MODEL_PATH, FACE_CASCADE_MODEL_PATH

    LABELS_MAPPING_FILE = WORKING_DIR + '/models/mapping.txt'
    TRAINED_MODEL_PATH = WORKING_DIR + '/models/emotion_finetune/model_finetune-119'
    FACE_CASCADE_MODEL_PATH = WORKING_DIR + '/models/haarcascade_frontalface_alt.xml'

    labelsDict = readMappingFromFile(LABELS_MAPPING_FILE)
    session, pred, imageTf = loadModelAndSession(TRAINED_MODEL_PATH)

    try:
        # Create an imshow window
        winName = "Facial Emotion Detector"

        # Create a VideoCapture object
        cap = cv2.VideoCapture(0)

        # Check if OpenCV is able to read feed from camera
        if (cap.isOpened() is False):
            print("Unable to connect to camera")
            sys.exit()

        # Get first frame
        ret, im = cap.read()

        if ret == True:
            size = im.shape[0:2]
        else:
            print("Unable to read frame")
            sys.exit()

        count = 0

        # Grab and process frames until the main window is closed by the user.

        # Parameters to control emokeywords
        time_window = 3
        emo_score_min = 0
        emo_thmin = 0.5
        score_decrement = -0.1
        max_variance = 0.3
        time_min_emo_staibility = 1

        # -----
        crono = time.time()
        crono_stability = time.time()
        nemotions = len(labelsDict)
        score_emo = np.zeros(nemotions, dtype=np.float)
        score_emo_prev = np.zeros(nemotions, dtype=np.float)
        score_emo_history = []
        #  for i in range(nemotions):
        #    score_emo_history.append([])
        time_emo_window = []
        init_emo = 0
        emokeywords = []
        emo_sent = 0
        emo_variance = 0
        emo_variance_history = []

        cv2.namedWindow(winName, cv2.WINDOW_NORMAL)
        cv2.resizeWindow(winName, 400, 300)

        while (True):
            # Grab a frame
            ret, im = cap.read()

            # Process frames at an interval of SKIP_FRAMES.
            # This value should be set depending on your system hardware
            # and camera fps.
            # To reduce computations, this value should be increased
            if (count % SKIP_FRAMES == 0):
                # Detect face
                img = detectFace(im)
                if img is None:
                    #print("\nimg is NONE!!")
                    # Decrease all score by 10%
                    if (len(score_emo_history) > 0):
                        score_emo_history.append([])
                        lscore = len(score_emo_history) - 1
                        for x in range(nemotions):
                            score_emo_history[lscore].append(score_decrement)
                        time_emo_window.append(time.time())
                else:
                    img = np.expand_dims(img, axis=0)
                    img = np.expand_dims(img, axis=3)
                    # stores the probabilities given by model's prediction
                    predictions = session.run([pred],
                                              feed_dict={imageTf: img})
                    # stores emotion as a string
                    #newMessage = labelsDict[np.argmax(predictions)]

                    # Collect detected emotion score
                    ap = predictions[0][0]
                    score_emo_history.append([])
                    lscore = len(score_emo_history) - 1
                    for x in range(len(ap)):
                        # print(str(x) + ") " + str(labelsDict[x]) + " --> " + str(ap[x]))
                        score_emo_history[lscore].append(ap[x])
                    time_emo_window.append(time.time())
                    #print("#1")

            # Calculate average emotions of last time window
            # Array of detected emotions to use as keywords for video reccomandetion
            emokeywords = []
            nvalid_emo = 0
            nscoreh = len(score_emo_history)
            for x in range(nemotions):
                score_emo_prev[x] = score_emo[x]
                score_emo[x] = 0
                if (nscoreh > 0):
                    for t in range(nscoreh):
                        if (score_emo_history[t][x] >= emo_score_min):
                            score_emo[x] += score_emo_history[t][x]
                    score_emo[x] /= nscoreh
                    # if score_emo[x] greater than emo_thmin it's used into the average and label collected
                    if (score_emo[x] > emo_thmin):
                        nvalid_emo = nvalid_emo + 1
                        emokeywords.append(labelsDict[x])

            #print("#2")
            # Calculate Score stability using only their over emo_thmin
            emo_variance = 0
            for x in range(nemotions):
                if(score_emo[x] > emo_thmin):
                    emo_variance += ((score_emo[x] - score_emo_prev[x]) * (score_emo[x] - score_emo_prev[x])) * 100
            if(nvalid_emo > 0):
                emo_variance /= nvalid_emo
            else:
                emo_variance = 10

            # Calculate average of last 5 variance detected
            emo_variance_history.append(emo_variance)
            if(len(emo_variance_history) > 5):
                del emo_variance_history[0]
            emo_variance = sum(emo_variance_history) / float(len(emo_variance_history))
            # I want low variation for at least
            if(emo_variance > max_variance):
                crono_stability = time.time()

            #print("#3")
            # Order keywords based on score
            emo_used = np.zeros(nemotions, dtype=np.float)
            emokeywords_ordered = []
            for x in range(nemotions):
                max_score_emo = 0
                imax_score_emo = -1
                for y in range(nemotions):
                    if (emo_used[y] == 0 and score_emo[y] > max_score_emo):
                        imax_score_emo = y
                        max_score_emo = score_emo[y]
                if (imax_score_emo >= 0):
                    emo_used[imax_score_emo] = 1
                    # I do not want to use Neutral
                    if(imax_score_emo < nemotions - 1):
                        emokeywords_ordered.append(labelsDict[imax_score_emo])

            #print("#3.5")
            # Check time to detect if time_window is passed and history to delete older ones than time_window
            tnow = time.time()
            for t in range(nscoreh):
                if (len(time_emo_window) > 0):
                    if (tnow - time_emo_window[0] > time_window):
                        del time_emo_window[0]
                        del score_emo_history[0]
                else:
                    break

            #print("#4")
            telapsed = time.time() - crono
            tstability = time.time() - crono_stability
            if (telapsed > time_window and len(time_emo_window) > 0 and nvalid_emo >= 1 and emo_sent == 0 and emo_variance < max_variance and tstability > time_min_emo_staibility):
                crono = time.time()
                print(emokeywords_ordered)
                emo_sent = 1
                init_emo = 1
                sys.stdout.flush()
            else:
                emokeywords = []
                emokeywords_ordered = []
                if(emo_sent == 1 and telapsed > time_window):
                    init_emo = 0
                    emo_sent = 0
                    crono = time.time()

            # if (init_emo == 0 or nvalid_emo <= 1):
            #     emokeywords = []
            #     emokeywords_ordered = []
            # # print("\n")
            # if (init_emo == 1 and nvalid_emo >= 1 and emo_sent == 0):
            #     print(emokeywords_ordered)
            #     # emo_sent = 1
            #     init_emo = 0
            #     sys.stdout.flush()

            #print("#5")
            # Show feedback
            # print("\n" + str(len(score_emo_history)))
            emo_var = emo_variance - 0.3
            if(emo_var > 10):
                emo_var = 10
            elif(emo_var < 0):
                emo_var = 0
            emo_var = (10 - emo_var) / 10
            #cv2.ellipse(im, (50, 30), (20, 20), 0, 0, int(emo_var * 360), (100, 200, 180), -1)
            telap = telapsed
            if(telap > time_window):
                telap = time_window
            telap = float(telap) / float(time_window)
            #cv2.ellipse(im, (150, 30), (20, 20), 0, 0, int(telap * 360), (200, 200, 100), -1)
            col_not_init = (100, 100, 100)
            col_minor = (255, 0, 0)
            col_major = (0, 255, 0)
            col_draw = col_not_init
            y_offset = 0
            cv2.line(im, (int(50 * emo_thmin / 2), y_offset), (int(50 * emo_thmin / 2), 15 + 30 * 7 + y_offset), (100, 200, 200), 1)
            cv2.line(im, (int(50 * emo_thmin / 2) - 20, int((2-max_variance)/2.0 * (15 + 30 * 7)) + y_offset), (int(50 * emo_thmin / 2) + 20, int((2-max_variance)/2.0 * (15 + 30 * 7)) + y_offset), (100, 200, 200), 1)
            cv2.line(im, (int(50 * emo_thmin / 2), y_offset), (int(50 * emo_thmin / 2), int(emo_var * (15 + 30 * 7)) + y_offset), (100, 200, 200), 5)
            for x in range(nemotions):
                col_draw = col_not_init
                if (init_emo > 0):
                    if (score_emo[x] >= emo_thmin):
                        col_draw = col_major
                    else:
                        col_draw = col_minor
                cv2.line(im, (0, 15 + 30 * x + y_offset), (int(50 * score_emo[x] / 2), 15 + 30 * x + y_offset), col_draw, 4)
                cv2.putText(im, str(labelsDict[x]) + " " + str(score_emo[x]), (50, 20 + 30 * x + y_offset), cv2.FONT_HERSHEY_PLAIN,
                            1.0, col_draw, 2)
                # print(str(x) + ") " + str(labelsDict[x]) + " --> " + str(score_emo[x]))
            #cv2.putText(im, str(emo_variance), (50, 20 + 30 * (len(score_emo) + 1) + y_offset), cv2.FONT_HERSHEY_PLAIN, 1.1, col_minor, 2)
            #cv2.putText(im, str(len(time_emo_window)), (50, 20 + 30 * (len(score_emo) + 2) + y_offset), cv2.FONT_HERSHEY_PLAIN, 1.1, col_minor, 2)

            # Display it all on the screen
            cv2.imshow(winName, im)
            # Wait for keypress
            key = cv2.waitKey(1) & 0xFF

            # Stop the program.
            if key == 27:  # ESC
                # If ESC is pressed, exit.
                session.close()
                sys.exit()
            # increment frame counter
            count = count + 1
            # Update message at an interval of 10 frames
            if (count == 10):
                count = 0
        cv2.destroyAllWindows()
        cap.release()
    except Exception as e:
        print(e)
