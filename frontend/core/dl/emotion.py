from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
import os
import logging
from math import ceil
import sys
import numpy as np
import tensorflow as tf

MEAN = 128.0


class EmotionNet:

  # Constructor declaration and initialization of weight decay
  def __init__(self, emotion_npy_path=None):
    #Initialize weight decay
    self.wd = 5e-4

  # Design of our Convolutional Neural Network
  def build(self, gray, train=False, num_classes=7, debug=False):
    gray = gray - MEAN
    self.conv_1 = self._conv_layer(gray, 64, 5, 1, "conv_1")
    self.pool1 = self._max_pool(self.conv_1, 3, 2, 'pool1', debug)

    self.conv_2 = self._conv_layer(self.pool1, 64, 5, 1, "conv_2")
    self.pool2 = self._max_pool(self.conv_2, 3, 2, 'pool2', debug)

    self.conv_3 = self._conv_layer(self.pool2, 128, 4, 1, "conv_3")
    self.dropout1 = self.conv_3

    if train:
      self.dropout1 = tf.nn.dropout(self.conv_3, 0.3)
    shape_temp = self.dropout1.get_shape().as_list()
    self.flat = tf.contrib.layers.flatten(self.dropout1)
    self.fc1 = self._fc_layer(self.flat, "fc1" , 4096)
    self.dropout2 = self.fc1
    if train:
      self.dropout2 = tf.nn.dropout(self.dropout2, 0.2)

    self.fc2 = self._fc_layer(self.dropout2, "fc2" , num_classes, relu=False)
    #output = tf.nn.softmax(self.fc2)
    print('done building train graph')
    return self.fc2
 
  # Function to create and add a max pooling layer to model
  def _max_pool(self, bottom, size, stride, name, debug):
    pool = tf.nn.max_pool(bottom, ksize=[1, size, size, 1], strides=[1, stride, stride, 1],
                padding='SAME', name=name)
    return pool

  # Function to create and add a convolution layer to model
  def _conv_layer(self, bottom, num_filters, size, stride, name):
    with tf.variable_scope(name) as scope:
      num_in_channels = bottom.get_shape()[3].value
      stddev = 0.01
      # create a filter for the convolution layer
      filt = self.get_conv_filter(num_in_channels, num_filters, size, stddev, name)
      # create a convolution layer
      conv = tf.nn.conv2d(bottom, filt, [1, stride, stride, 1], padding='SAME')
      # create a bias node for the convolution layer
      conv_biases = self.get_bias(name, num_filters)
      # add bias with the convolution output
      bias = tf.nn.bias_add(conv, conv_biases)
      # pass the output to ReLU Activation
      relu = tf.nn.relu(bias)
      return relu

  # Function to create and add a fully connected layer to model
  def _fc_layer(self, bottom, name, num_filters,
          relu=True, debug=False):
    with tf.variable_scope(name) as scope:
      num_in_channels = bottom.get_shape()[1].value
      stddev = 0.005
      # create weights tensor for the fully connected layer
      filt = self.get_fc_weight(name,stddev,num_in_channels, num_filters)
      # create a bias tensor for the fully connected layer
      biases = self.get_bias(name, num_filters)
      # multiply the previous layer output with the weights
      output = tf.matmul(bottom, filt,name=scope.name)
      # do addition of the previous result with bias of the layer
      bias = tf.nn.bias_add(output, biases)

      # pass output through ReLU activation function if expression is True
      if relu:
        bias = tf.nn.relu(bias)
      #print value of bias if expression is True
      if debug:
        bias = tf.Print(bias, [tf.shape(bias)],
                message='Shape of %s' % name,
                summarize=4, first_n=1)
      return bias

  # create filter for a convolution layer
  def get_conv_filter(self, num_in_channels, num_filters, size, stddev, name):
    # create a initializer for the filter
    init = tf.truncated_normal_initializer(stddev = stddev,dtype=tf.float32)
    shape = [size, size, num_in_channels, num_filters]
    print('Layer name: %s' % name)
    print('Layer shape: %s' % str(shape))
    # create a filter for the convolution layer
    var = tf.get_variable(name="filter", initializer=init, shape=shape)
    if not tf.get_variable_scope().reuse:
      # multiply L2 loss with the weight decay 
      weight_decay = tf.multiply(tf.nn.l2_loss(var), self.wd,
                     name='weight_loss')
      # add weight_decay to Regularization Loss collection
      tf.add_to_collection(tf.GraphKeys.REGULARIZATION_LOSSES,
                 weight_decay)
    return var

  # create bias variables for a convolution layer with value set to 0
  def get_bias(self, name, num_filters):
    shape = [num_filters]
    # create a initialier that creates a tensor with constant value
    init = tf.constant_initializer(value=0.0,
                     dtype=tf.float32)
    # create a tensor of a particular shape and initialize it with a initializer
    return tf.get_variable(name="biases", initializer=init, shape=shape)

  # initialize weights for a fully connected layer  
  # specify the weight decay to be done by regularization loss
  def get_fc_weight(self, name, stddev, num_in_channels, num_filters):
    # create a initializer with normal distribution with a standard deviation 
    init = tf.truncated_normal_initializer(stddev = stddev,
                     dtype=tf.float32)
    shape = [num_in_channels, num_filters]
    #create a weights tensor
    var = tf.get_variable(name="weights", initializer=init, shape=shape)
    
    if not tf.get_variable_scope().reuse:
      weight_decay = tf.multiply(tf.nn.l2_loss(var), self.wd,
                     name='weight_loss')
      tf.add_to_collection(tf.GraphKeys.REGULARIZATION_LOSSES,
                 weight_decay)
    return var

  # create bias variables for a fully connected layer
  def _bias_variable(self, shape, constant=0.0):
    # create a initialier that creates tensor with constant value
    initializer = tf.constant_initializer(constant)

    # create a tensor of a particular shape and initialize it with a initializer
    return tf.get_variable(name='biases', shape=shape,
                 initializer=initializer)

