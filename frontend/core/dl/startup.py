import os
import sys
from os import path
from inspect import getsourcefile
from os.path import abspath
import fastvidfeed_prediction

if __name__ == '__main__' and __package__ is None:
    # sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
    # os.path.dirname(abspath(getsourcefile(lambda: 0)))
    fastvidfeed_prediction.main(os.path.dirname(abspath(getsourcefile(lambda: 0))))