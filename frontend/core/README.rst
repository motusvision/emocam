***************************************************************
An emotion recognition system using convolution neural network.
***************************************************************


## Instructions
   1. Build and install opencv from scratch
   2. Install other dependencies `pip install -r requirements.txt`
   2. Copy model files from google drive link into directory `dl/models/`
   3. Run startup.py to test: `python dl/startup.py`