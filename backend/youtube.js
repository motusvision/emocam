var google = require('googleapis'),
    OAuth2 = google.auth.OAuth2;

module.exports = function(googleAuth) {
  this.oauth2Client = new OAuth2(
      googleAuth.clientID,
      googleAuth.clientSecret,
      googleAuth.callbackURL
  );
  console.log("Youtube authentication initialized");
  this.subscriptions = function (req, res) {
    oauth2Client.credentials = {
        access_token: req.access_token,
        refresh_token: req.refresh_token
    };

    google.youtube({
        version: 'v3',
        auth: oauth2Client
    }).subscriptions.list({
        part: 'snippet',
        mine: true,
        headers: {}
    }, function(err, data, response) {
        if (err) {
            console.error('Error: ' + err);
            res.json({
                status: "error"
            });
        }
        if (data) {
            console.log(data);
            res.json({
                status: "ok",
                data: data
            });
        }
        if (response) {
            console.log('Status code: ' + response.statusCode);
        }
    });
  }

  return this;
};
