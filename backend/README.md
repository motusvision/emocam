## Instructions to build backend
#### 1. Download mongodb 3.4.9 
  - windows: http://downloads.mongodb.org/win64/mongodb-win64-x86_64-3.4.9.zip
  - linux: http://downloads.mongodb.org/linux/mongodb-linux-x86_64-3.4.9.tgz

#### 2. Create a data directory (i.e. path/to/data) and start mongodb
  - windows: ```mongod.exe -dbpath "path/to/data""``` 
  - linux: ```mongod -dbpath "path/to/data""```
#### 3. Install node v8.4.0 (https://nodejs.org/download/release/v8.4.0/)
#### 4. Run the folowing command 
  - ```npm install``` from this directory. 
  - All dependencies should be installed from ```package.json```
  - Note: If there is an error saying ```errno EAI_AGAIN```, it's network issue, try again.

#### 5. Start the server 
  - ```node server.js```
  - Now server is started. 
  - Note: If mongodb is not started, then server will throw an exception.
   