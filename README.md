##  Emotion Based Video Recommendation System
---------------------------------------------
MOTUS Lab - CV4Faces Course 2017

Authors: Md. Saiful Islam Sayef, Rudy Melli, Amit Pandey, Srikanth Vidapanakal

## Introduction

In this paper, we present the report of our final project for the course _CV4Faces_ from LearnOpenCV.com conducted by Satya Mallik, PhD.

## Idea

YouTube and other online video sharing websites contain millions of videos. They are continuously collecting user data i.e. watch history, search history etc to recommend users the related videos,  even from the ones those have been released recently and the recommendation engine thinks suitable for him/her.  We want to propose a new approach to video recommendation based on emotion read from the face of the person in front of the screen, which we think will add an extra value to the video recommendation system. We can collect data based on the feeling and can determine which type of videos one likes to watch when he/she is in such mood/feeling. So the final idea is to prototype such video suggestion/recommendation system that works based on emotion detection.

## Technical Description

The project we have prototyped is in 3 parts, image processing core, backend and UI. The processing core is based on fast video feed prediction method provided as example in week 11 of the course which uses a deep neural network trained model to classify the emotion from one&#39;s face into 7 classes: Neutral, Sad, Happy, Fear, Surprise, Anger and Disgust.

We tried to fine tune the model by changing some of the hyperparameters and ended up with negligible amount of improvement since we really don&#39;t know the insights that could improve the accuracy. One solution could be more robust data. Since it is a time consuming process, we kept that aside and started implementing the idea of the proposal.

The model seemed quite good for our implementation, only one challenge was to normalize the deviations. For example, if there is a sad face, it predicts sad for maximum times but there are some other emotions that also been predicted. So we need to filter these emotions&#39; spikes, hence we use not only the information detected in a particular frame but collect informations for a certain duration of time and accumulate them to provide a more reliable emotion keyword. To achieve this we calculate a time window average for each of the 7 emotions&#39; scores to normalize the deviation of the scores. Then we send results only if it is stable for a certain duration of time, i.e. if we continuously change face appearance, the data detected is not feasible. Then we calculate the variance of the system in the last 5 detections and send to the keyword to UI only if the variance is under a certain threshold for at least 1/2 seconds. Thus we achieve a good emotion detection of the person in front of the camera.

Here are some examples of our implementation:

[![CHECK OUT HERE](https://img.youtube.com/vi/bI6B1flEp_o/0.jpg)](https://www.youtube.com/watch?v=bI6B1flEp_o)

We are using NodeJS and MongoDB as our backend purposes. Since we have integrated Google OAuth API to retrieve related videos of particular video for an user (although this part is partially done), we have used MongoDB to save user auth tokens for a particular session. We will apply our own authentication (signup/login) process in future. However, since a database is necessary for any type of storage for an application, we started with MongoDB.

We wanted our application to be run anywhere regardless of OS. That&#39;s why we used Electron as the frontend application. Electron communicates with in-app core processing module and remote backend.

## How to run the application

Since we fell in time shortage, we could not able to package the whole application in a single executable file. Hence we request you to follow the procedures given below to run the application.

1. Download the project from bitbucket [https://bitbucket.org/motusvision/emocam](https://bitbucket.org/motusvision/emocam)
2. Download the model files from [here](https://drive.google.com/open?id=0B_KD4D-vWVxxVVMxWEdoaVlKSDQ) and copy into the subfolder `frontend/core/dl/models/`
3. Activate virtual environment that contains tensorflow and opencv setup
4. Test core launching `python frontend/core/dl/startup.py` (If camera window appears, it&#39;s all good)
5. Instructions to build backend

    i) Download mongodb 3.4.9
    - windows: http://downloads.mongodb.org/win64/mongodb-win64-x86_64-3.4.9.zip
    - linux: [http://downloads.mongodb.org/linux/mongodb-linux-x86\_64-3.4.9.tgz](http://downloads.mongodb.org/linux/mongodb-linux-x86_64-3.4.9.tgz)

    ii) Create a data directory (i.e. path/to/data) and start mongodb
    - windows: `mongod.exe -dbpath "path/to/data"`
    - linux: `mongod -dbpath "path/to/data"`

    iii) Install node v8.4.0 (https://nodejs.org/download/release/v8.4.0/)
    
    iv) Run the command `npm install` from backend/ directory.
    - All dependencies should be installed from ```package.json```
    - If there is an error saying ```errno EAI_AGAIN```, it's network issue, try again.

    v) Start the server : **node server.js**
    - Now server is started.
    - Note: If mongodb is not started, then server will throw an exception.

6. Instructions to build frontend

- If backend/server is ready
    i) Go to directory `frontend/ui/`
    ii)  Run the command   `npm install`
    - All dependencies should be installed from `package.json`
    - If there is an error saying `errno EAI_AGAIN`, it&#39;s network issue, try again.

    iii)  Run the command `npm install -g electron` to install electron globally
    iv) Start the application: `electron index.js`

- Now app is started and it launch the deep network part too

**This instruction set has been tested for both windows and linux machine and works fine.**

## Video of the project

[https://youtu.be/bI6B1flEp\_o](https://youtu.be/bI6B1flEp_o)

Uncutted screen capture: [https://youtu.be/jFRcA-v3zps](https://youtu.be/jFRcA-v3zps)

## Future works

Videos now we suggest are hard coded which are pre-categorized from an arbitrary list collected from internet. We will use some other NLP techniques to retrieve more related  videos according to users&#39;  moods, emotions or scientific studies related also to psychology [1][2][3].

## Results and Conclusion

The project is simple but works correctly and it's funny to use. But real time integration seems cumbersome and needs to be improved in accuracy a lot. But we think making product using neural network as backend is fun. May be this could be distributed as mobile app or chrome plugin when someone is connected to youtube.

## References

1. "The influence of user&#39;s emotions in recommender systems for decision making processes", Marco Polignano, Proc. of CHItaly 2015 Doctoral Consortium, Rome (Italy), September 28th 2015
2. "Towards enhanced video access and recommendation through emotions", Eva Oliveira, Nuno Magalh�es Ribeiro, Teresa Chambel, CHI 2009, April 4 � 9, 2009, Boston, MA, USA, ACM
3. "Video access and interaction based on emotions", Eva Oliveira, EuroITV, June 29�1Jul, 2011, Lisbon, Portugal.

